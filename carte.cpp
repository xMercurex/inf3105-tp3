/*  INF3105 - Structures de données et algorithmes
 *  UQAM / Département d'informatique
 *  Automne 2014
 *  Squelette pour le TP3
 * FREJ31088803
 */

#include "carte.h"
#include <limits>

void Carte::ajouterLieu(const string& nomlieu, const Coordonnee& c) {
    Lieu& lieu = lieux[nomlieu];
    lieu.coor = c;
    lieu.nom = nomlieu;
}

void Carte::ajouterRoute(const string& nomroute, const list<string>& route) {
    list<string>::const_iterator iter1 = route.begin();
    if (iter1 == route.end()) return;
    list<string>::const_iterator iter2 = iter1;
    ++iter2;
    while (iter2 != route.end()) {
        Route &r = lieux.at(*iter1).voisins_route[*iter2];
        r.nom = nomroute;
        r.distance = 0;
        iter1 = iter2;
        ++iter2;
    }
    /*
     * Sauter les noeuds intermédiaire semble une bonne idée, mais cela cause des
     * problème au niveau de la distance.
     * Ce code donne le bon chemin en terme de route, mais donne des distance
     * inférieur à ceux espérer et moins de noeuds.
     * semble aussi plus rapide
    for(list<string>::const_iterator iter1 = route.begin(); iter1!=route.end();++iter1){
        list<string>::const_iterator iter2 = iter1;
        ++iter2;
            while (iter2 != route.end()) {
                
            Route &r = lieux.at(*iter1).voisins_route[*iter2];
            r.nom=nomroute;
            ++iter2;
        }
     }
     */
    
}

double Carte::calculerTrajet(const string& nomorigine, list<string> nomsdestinations,
        list<string>& out_cheminnoeuds, list<string>& out_cheminroutes) {
    nomsdestinations = nettoyage(nomorigine, nomsdestinations);
    if(nomsdestinations.empty()){
        nomsdestinations.push_back(nomorigine);
    }
    assert(lieux.count(nomorigine));
    Lieu origine = lieux.at(nomorigine);


    double total = calcul_chemin(origine, origine, nomsdestinations);
    int n=0;
    for (list<string>::iterator iter = nomsdestinations.begin();
            iter != nomsdestinations.end(); iter++) {
        assert(lieux.count(*iter));
        total+=calcul_chemin(origine, lieux.at(*iter), nomsdestinations);
        n++;
    }
    total=total/n+1;
    Chemin optimal = chemin_optimal(nomorigine, nomorigine, nomsdestinations, 0, total);
    copie_chemin(optimal.chemin_lieu, out_cheminnoeuds);
    copie_chemin(optimal.chemin_route, out_cheminroutes);
    return optimal.distance;

}

list<string> Carte::nettoyage(string nomorigine, list<string>& in){
    list<string> out;
    for (list<string>::iterator iter = in.begin();iter != in.end(); ++iter) {
        bool dejacopie=0;
        if(*iter != nomorigine){
            for (list<string>::iterator iter2 = out.begin();iter2 != out.end(); ++iter2){
                if(*iter==*iter2)
                    dejacopie=1;
            }
            if(!dejacopie){
                out.push_back(*iter);
            }
        }
        
    }
    return out;
}

void Carte::copie_chemin(list<string> in_chemin, list<string>& out_chemin) {
    string nom;

    while (!in_chemin.empty()) {
        nom = in_chemin.front();
        if (out_chemin.empty()) {
            out_chemin.push_front(nom);
        } else if (nom != out_chemin.front() && nom != "") {
            out_chemin.push_front(nom);
        }
        in_chemin.pop_front();

    }
}

double Carte::calcul_chemin(Lieu& debut, Lieu& origine, list<string> liste_destinations) {
    if (liste_parcours.count(origine.nom) == 0) {
        Sommet* sommet = &liste_parcours[origine.nom];
        init_sommet(sommet, origine);
    }
    assert(liste_parcours.count(origine.nom));
    Sommet& sommet = liste_parcours.at(origine.nom);
    double total =0;
    if (sommet.chemin_precalcul.count(debut.nom) == 0 && origine.nom!=debut.nom) {
        Chemin& c = sommet.chemin_precalcul[debut.nom];
        c.distance = chemin_deux_points(origine, debut.nom, c);
        total+=c.distance;
    }else if(origine.nom!=debut.nom){
        total+=sommet.chemin_precalcul.at(debut.nom).distance;
    }
    

    for (list<string>::iterator iter = liste_destinations.begin();
            iter != liste_destinations.end(); ++iter) {
        if (liste_parcours.count(*iter) == 0) {
            Sommet* s2 = &liste_parcours[*iter];
            assert(lieux.count(*iter));
            init_sommet(s2, lieux.at(*iter));
        }
        if (sommet.chemin_precalcul.count(*iter) == 0&& (origine.nom!=*iter || debut.nom==*iter)) {

            Chemin& c = sommet.chemin_precalcul[*iter];

            c.distance = chemin_deux_points(origine, *iter, c);
            total += c.distance;
        }else if(origine.nom!=*iter){
            assert(sommet.chemin_precalcul.count(*iter));
            total+=sommet.chemin_precalcul.at(*iter).distance;
        }

    }
    return total;

}

typename Carte::Chemin Carte::chemin_optimal(string debut, string nomorigine,
        list<string> liste_destinations, double total, double plafond) {
    Chemin min;
    min.distance = plafond;
    assert(liste_parcours.count(nomorigine));
    Sommet& origine = liste_parcours.at(nomorigine);
    if (liste_destinations.empty()) {
        Chemin chemin;
        assert(origine.chemin_precalcul.count(debut) != 0);
        chemin = origine.chemin_precalcul.at(debut);
        return chemin;
    }
    for (list<string>::iterator iter = liste_destinations.begin(); iter != liste_destinations.end(); iter++) {
        list<string> dest = liste_destinations;
        dest.remove(*iter);
        assert(origine.chemin_precalcul.count(*iter));
        Chemin& c = origine.chemin_precalcul.at(*iter);
        if (c.distance > min.distance) continue;
        double total2 = total + c.distance;
        if (total2 < plafond) {
            Chemin chemin = chemin_optimal(debut, *iter, dest, total2, plafond);
            assert(origine.chemin_precalcul.count(debut) != 0);
            chemin.distance += c.distance;
            if (chemin.distance < min.distance) {
                ajouter_chemin(c.chemin_lieu, chemin.chemin_lieu);
                ajouter_chemin(c.chemin_route, chemin.chemin_route);
                min = chemin;
                plafond = total2 + min.distance;
            }
        }
    }
    return min;
}

void Carte::ajouter_chemin(list<string> in, list<string>& out) {
    while (!in.empty()) {
        out.push_back(in.front());
        in.pop_front();
    }
}

double Carte::chemin_deux_points(Lieu& lieu_origine, string nomdestination,
        Chemin& chemin) {
    priority_queue<Noeud> lieux_prioritaire;
    std::unordered_map<string, Noeud> map_noeud;
    
    assert(lieux.count(nomdestination) == 1);
    Coordonnee final_coor = lieux.at(nomdestination).coor;

    Noeud* origine = &map_noeud[lieu_origine.nom];
    init_noeud(origine, lieu_origine);
    origine->distance = origine->coor->distance(final_coor);


    lieux_prioritaire.push(*origine);
    assert(!lieux_prioritaire.empty());
    Noeud courrant;
    while (!lieux_prioritaire.empty()) {
        courrant = lieux_prioritaire.top();
        lieux_prioritaire.pop();
        if (courrant.nom == nomdestination) {
            string parent = courrant.nom;
            Noeud *n = &courrant;
            while (parent != "") {
                assert(map_noeud.count(parent) != 0);
                chemin.chemin_route.push_back(n->route);
                chemin.chemin_lieu.push_back(parent);
                if (parent == origine->nom) break;
                n = n->parent;
                parent = n->nom;
            }
            assert(courrant.distance_parcourue != numeric_limits<double>::infinity());
            return courrant.distance_parcourue;
        }
        editer_noeud(courrant, lieux_prioritaire, map_noeud, final_coor);
    }
    assert(1 == 0); // ne devrait jamais aller ici
    return numeric_limits<double>::infinity();

}

void Carte::editer_noeud(Noeud courrant, priority_queue<Noeud> &lieux_prioritaire,
        std::unordered_map<string, Noeud>& map_noeud, Coordonnee final_coor) {
    Noeud* enfant;
    double distance;
    double distance_parcourue;
    for (map<string, Route>::iterator iter = courrant.voisins_route->begin();
            iter != courrant.voisins_route->end(); ++iter) {
        if (map_noeud.count(iter->first) == 0) {
            Noeud *noeud = &map_noeud[iter->first];
            assert(lieux.count(iter->first));
            init_noeud(noeud, lieux.at(iter->first));
            lieux_prioritaire.push(*noeud);

        }

        assert(map_noeud.count(iter->first));
        enfant = &map_noeud.at(iter->first);

        if (iter->second.distance == 0) {
            iter->second.distance = enfant->coor->distance(*courrant.coor);
        }

        distance_parcourue = courrant.distance_parcourue + iter->second.distance;

        distance = courrant.distance_parcourue+iter->second.distance + enfant->coor->distance(final_coor);
        
        string parent = "";
        if (courrant.parent) {
            parent = courrant.parent->nom;
        }
        if (distance < enfant->distance && parent != enfant->nom) {
            enfant->distance_parcourue = distance_parcourue;
            enfant->distance = distance;
            enfant->parent = &map_noeud.at(courrant.nom);
            enfant->route = iter->second.nom;
            lieux_prioritaire.push(*enfant);
        }
    }
}

void Carte::init_noeud(Noeud* noeud, Lieu &l) {
    noeud->distance = numeric_limits<double>::infinity();
    noeud->distance_parcourue = 0;
    noeud->nom = l.nom;
    noeud->coor = &l.coor;
    noeud->voisins_route = &l.voisins_route;
}

void Carte::init_sommet(Sommet* sommet, Lieu& original) {
    sommet->nom = original.nom;
    sommet->coor = original.coor;
    //route inutile;
}

/* Lire une carte. */
istream& operator >>(istream& is, Carte& carte) {
    // Lire les lieux
    carte.lieux.rehash(49999);
    //84991
    //99991
    //carte.lieux.rehash(499897); // 500milles
    unsigned long int n;
    while (is) {
        n++;
        string nomlieu;
        is >> nomlieu;
        if (nomlieu == "---") break;
        Coordonnee coor;
        is >> coor;
        carte.ajouterLieu(nomlieu, coor);
    }
    
    // Lire les routes
    while (is) {
        string nomroute;
        is >> nomroute;
        if (nomroute == "---" || nomroute == "" || !is) break;

        char deuxpoints;
        is >> deuxpoints;
        assert(deuxpoints == ':');

        std::list<std::string> listenomslieux;

        string nomlieu;
        while (is) {
            is >> nomlieu;
            if (nomlieu == ";") break;
            assert(nomlieu != ":");
            assert(nomlieu.find(";") == string::npos);
            listenomslieux.push_back(nomlieu);
        }

        assert(nomlieu == ";");
        carte.ajouterRoute(nomroute, listenomslieux);
    }

    return is;
}

