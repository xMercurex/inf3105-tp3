/*  INF3105 - Structures de données et algorithmes
 *  UQAM / Département d'informatique
 *  Automne 2014
 *  FREJ31088803
 */

#ifndef CARTE_HEADER
#define CARTE_HEADER
#define NDEBUG
#include <assert.h>
#include <istream>
#include <list>
#include <string>
#include <queue>
#include <map>
#include <set>
#include <unordered_map>
#include "coordonnee.h"
using namespace std;

class Carte {
public:
    void ajouterLieu(const string& nom, const Coordonnee& c);
    void ajouterRoute(const string& nom, const list<string>& noms);

    double calculerTrajet(const string& origine, list<string> destination,
            std::list<string>& out_cheminnoeuds, std::list<string>& out_cheminroutes);

private:
    class Noeud;

    class Chemin {
    public:
        double distance;

        bool operator>(const Chemin& c) const {
            return distance < c.distance;
        }

        bool operator>=(const Chemin& c) const {
            return distance <= c.distance;
        }

        bool operator<(const Chemin& c) const {
            return distance > c.distance;
        }

        bool operator <=(const Chemin& c) const {
            return distance >= c.distance;
        }
        list<string> chemin_lieu;
        list<string> chemin_route;
    };

    class Route {
    public:
        string nom;
        double distance;
    };

    class Lieu {
    public:
        string nom;

        bool operator ==(const Lieu & l) const {
            return coor == l.coor;
        }
        Coordonnee coor; 
        map<string, Route> voisins_route;
    };

    class Sommet : public Lieu {
    public:
        unordered_map<string, Chemin> chemin_precalcul;
    };

    class Noeud {
    public:

        bool operator>(const Noeud & n) const {
            return distance < n.distance;
        }

        bool operator>=(const Noeud & n) const {
            return distance <= n.distance;
        }

        bool operator<(const Noeud & n) const {
            return distance > n.distance;
        }

        bool operator <=(const Noeud & n) const {
            return distance >= n.distance;
        }
        double distance;
        double distance_parcourue;
        Noeud* parent;
        map<string, Route>* voisins_route;
        Coordonnee * coor;
        string route;
        string nom;

    };

    double chemin_deux_points(Lieu&, string, Chemin& chemin);
   
    void editer_noeud(Noeud, priority_queue<Noeud> &, std::unordered_map<string, Noeud>&, Coordonnee);
   
    void init_noeud(Noeud*, Lieu&);

    void init_sommet(Sommet*, Lieu&);

    void copie_chemin(list<string>, list<string>&);
    list<string> nettoyage(string, list<string>&);

    Chemin chemin_optimal(string, string, list<string>, double, double);

    double calcul_chemin(Lieu&, Lieu&, list<string>);

    void ajouter_chemin(list<string>, list<string>&);

    std::unordered_map<string, Lieu> lieux;
    std::unordered_map<string, Sommet> liste_parcours;






    friend istream& operator >>(istream& is, Carte& carte);
};

#endif

